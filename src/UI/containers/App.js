import React from 'react';
import { connect } from 'react-redux';
import { login, joinChat  } from '../../redux/actions';
import Header from '../components/Header';
import ChatList from '../components/ChatList';
import Chatroom from './Chatroom';
import LoginBox from '../components/LoginBox';
import { __ } from '../../misc/util';
import { runtime } from 'webextension-polyfill';

const App = ({ addr, topic, login, enterChatroom, messages, subscriptions, connected }) => {
	// console.log('Rendering.....................');
	// console.log('all my arguments:', addr, topic, login, createMessage, enterChatroom, messages);
	const isLoggedIn = addr != null;
	const isSubscribing = ( topic && subscriptions[topic] );
	const loading = ( isLoggedIn && !connected );
	return (
		<div className="app">
			<div className={`app-container ${loading ? 'loading' : ''}`}>
				<div className={loading ? 'absolute loading-description' : 'hidden'}>
					<i className="loader" />
					<p>
						{ __('Connecting...') }
					</p>
					<p className="description">
						{ __('If it seems stuck for a long, long time, then it probably is.') }
					</p>
					<button type="button" onClick={() => runtime.reload()}>
						{__('Reload')}
					</button>
				</div>
				{ isLoggedIn &&
					<Header
						topic={topic}
						enterChatroom={enterChatroom}
						subscribing={isSubscribing}
						connected={connected}
					/> }
				<div className={addr ? ( topic == null ? 'chatlist-container' : 'chatroom'  ) : 'login'}>
					{ isLoggedIn ?
						( topic ?
							<Chatroom />
							:
							<ChatList
								messages={messages}
								enterChatroom={enterChatroom}
							/>
						)
						:
						<LoginBox login={login} />
					}
				</div>
			</div>
		</div>
	);
};

const mapStateToProps = state => ({
	addr: state.login ? state.login.addr : {},
	topic: state.topic,
	messages: state.messages,
	subscriptions: state.subscriptions,
	connected: state.login && state.login.connected
});

const mapDispatchToProps = dispatch => ({
	login: credentials => dispatch(login(credentials)),
	enterChatroom: topic => dispatch(joinChat(topic))
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(App);
