import './index.css';
import renderApp from '../UI';
import { browserAction } from 'webextension-polyfill';

// Reset badge text on popup open
browserAction.setBadgeText({ text: '' });
renderApp();
